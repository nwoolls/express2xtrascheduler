﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Xml;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;

namespace Express2XtraScheduler.Core
{
    static class ExpressSchedulerConversion
    {
        public static WeekDays DayTypeToWeekDays(this ExpressScheduler.EventRecurrenceInfo sourceRecurrenceInfo)
        {
            switch (sourceRecurrenceInfo.DayType)
            {
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Day:
                    return OccurDaysToWeekDays(sourceRecurrenceInfo);
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.EveryDay:
                    return WeekDays.EveryDay;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Friday:
                    return WeekDays.Friday;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Monday:
                    return WeekDays.Monday;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Saturday:
                    return WeekDays.Saturday;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Sunday:
                    return WeekDays.Sunday;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Thursday:
                    return WeekDays.Thursday;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Tuesday:
                    return WeekDays.Tuesday;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Wednesday:
                    return WeekDays.Wednesday;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.WeekDay:
                    return WeekDays.WorkDays;
                case ExpressScheduler.EventRecurrenceInfo.DayTypeKind.WeekEndDay:
                    return WeekDays.WeekendDays;
            }

            return 0;
        }

        private static WeekDays OccurDaysToWeekDays(ExpressScheduler.EventRecurrenceInfo sourceRecurrenceInfo)
        {
            WeekDays destinationWeekDays = 0;

            if ((sourceRecurrenceInfo.OccurDays & ExpressScheduler.EventRecurrenceInfo.Days.Friday) == ExpressScheduler.EventRecurrenceInfo.Days.Friday)
                destinationWeekDays = destinationWeekDays | WeekDays.Friday;
            if ((sourceRecurrenceInfo.OccurDays & ExpressScheduler.EventRecurrenceInfo.Days.Monday) == ExpressScheduler.EventRecurrenceInfo.Days.Monday)
                destinationWeekDays = destinationWeekDays | WeekDays.Monday;
            if ((sourceRecurrenceInfo.OccurDays & ExpressScheduler.EventRecurrenceInfo.Days.Saturday) == ExpressScheduler.EventRecurrenceInfo.Days.Saturday)
                destinationWeekDays = destinationWeekDays | WeekDays.Saturday;
            if ((sourceRecurrenceInfo.OccurDays & ExpressScheduler.EventRecurrenceInfo.Days.Sunday) == ExpressScheduler.EventRecurrenceInfo.Days.Sunday)
                destinationWeekDays = destinationWeekDays | WeekDays.Sunday;
            if ((sourceRecurrenceInfo.OccurDays & ExpressScheduler.EventRecurrenceInfo.Days.Thursday) == ExpressScheduler.EventRecurrenceInfo.Days.Thursday)
                destinationWeekDays = destinationWeekDays | WeekDays.Thursday;
            if ((sourceRecurrenceInfo.OccurDays & ExpressScheduler.EventRecurrenceInfo.Days.Tuesday) == ExpressScheduler.EventRecurrenceInfo.Days.Tuesday)
                destinationWeekDays = destinationWeekDays | WeekDays.Tuesday;
            if ((sourceRecurrenceInfo.OccurDays & ExpressScheduler.EventRecurrenceInfo.Days.Wednesday) == ExpressScheduler.EventRecurrenceInfo.Days.Wednesday)
                destinationWeekDays = destinationWeekDays | WeekDays.Wednesday;

            return destinationWeekDays;
        }
        
        public static WeekOfMonth DayInfoToWeekOfMonth(this ExpressScheduler.EventRecurrenceInfo sourceRecurrenceInfo)
        {
            if (sourceRecurrenceInfo.DayType == ExpressScheduler.EventRecurrenceInfo.DayTypeKind.Day)
                return WeekOfMonth.None;
            else
                return (WeekOfMonth)sourceRecurrenceInfo.DayNumber;
        }

        public static RecurrenceType ToRecurrenceType(this ExpressScheduler.EventRecurrenceInfo.RecurrenceKind sourceRecurrenceKind)
        {
            RecurrenceType destinationRecurrenceType = 0;

            switch (sourceRecurrenceKind)
            {
                case ExpressScheduler.EventRecurrenceInfo.RecurrenceKind.Daily:
                    destinationRecurrenceType = RecurrenceType.Daily;
                    break;
                case ExpressScheduler.EventRecurrenceInfo.RecurrenceKind.Monthly:
                    destinationRecurrenceType = RecurrenceType.Monthly;
                    break;
                case ExpressScheduler.EventRecurrenceInfo.RecurrenceKind.Weekly:
                    destinationRecurrenceType = RecurrenceType.Weekly;
                    break;
                case ExpressScheduler.EventRecurrenceInfo.RecurrenceKind.Yearly:
                    destinationRecurrenceType = RecurrenceType.Yearly;
                    break;
            }

            return destinationRecurrenceType;
        }

        public static AppointmentType ToAppointmentType(this ExpressScheduler.EventType sourceEventType)
        {
            switch (sourceEventType)
            {
                case ExpressScheduler.EventType.None:
                    return AppointmentType.Normal;
                case ExpressScheduler.EventType.Pattern:
                    return AppointmentType.Pattern;
                case ExpressScheduler.EventType.Occurrence:
                    return AppointmentType.Occurrence;
                case ExpressScheduler.EventType.Exception:
                    return AppointmentType.DeletedOccurrence;
                case ExpressScheduler.EventType.Custom:
                    return AppointmentType.ChangedOccurrence;
            }

            return 0;
        }

        public static string ResourceIDBlobToXML(string resourceIDBlob)
        {
            string resources = null;
            ResourceIDBlobToString(resourceIDBlob, out resources);
            if (resources == null)
                resources = resourceIDBlob;

            string[] resourceArray = resources.Split(',');

            ResourceIdCollection idCollection = new ResourceIdCollection();
            foreach (string resourceID in resourceArray)
                idCollection.Add(int.Parse(resourceID));

            return new AppointmentResourceIdCollectionContextElement(idCollection).ValueToString();
        }

        [DllImport("ExpressSchedulerInterop.dll", CallingConvention = CallingConvention.StdCall)]
        static extern void ResourceIDBlobToString([MarshalAs(UnmanagedType.BStr)] string resourceID, [Out, MarshalAs(UnmanagedType.BStr)] out string result);

        //http://stackoverflow.com/a/2887/33112
        public static void ToStructure(this byte[] byteArray, ref object structure)
        {
            int length = Marshal.SizeOf(structure);
            IntPtr handle = Marshal.AllocHGlobal(length);
            Marshal.Copy(byteArray, 0, handle, length);
            structure = Marshal.PtrToStructure(handle, structure.GetType());
            Marshal.FreeHGlobal(handle);
        }

        public static object ColorToColorIndex(int color)
        {
            // with ExpressScheduler the labels are stored as their color rather than the index of the label in the series
            // XtraScheduler stores the index
            int[] labelColors = new int[] { 536870912, 8689404, 14982788, 6610596, 13952740, 7649020, 16051844, 8703700, 16033476, 12897956, 7661308 };
            List<int> labelList = new List<int>(labelColors);

            return labelList.IndexOf(color);
        }

        //http://stackoverflow.com/a/8314864/33112
        public static Color DelphiColorToColor(uint delphiColor)
        {
            switch ((delphiColor >> 24) & 0xFF)
            {
                case 0x01: // Indexed
                case 0xFF: // Error
                    return Color.Transparent;

                case 0x80: // System
                    return Color.FromKnownColor((KnownColor)(delphiColor & 0xFFFFFF));

                default:
                    var r = (int)(delphiColor & 0xFF);
                    var g = (int)((delphiColor >> 8) & 0xFF);
                    var b = (int)((delphiColor >> 16) & 0xFF);
                    return Color.FromArgb(r, g, b);
            }
        }

        public static string ReminderInfoToXML(DbDataReader selectReader, string dateColumn, string minutesColumn)
        {
            ReminderInfoCollection reminders = new ReminderInfoCollection();
            ReminderInfo reminderInfo = new ReminderInfo();
            reminders.Add(reminderInfo);

            if (!string.IsNullOrEmpty(dateColumn))
            {
                object dateValue = selectReader[dateColumn];

                //witnessed this as both a DateTime from an MDB and a Double from Advantage DB
                if (dateValue is DateTime)
                    reminderInfo.AlertTime = (DateTime)dateValue;
                else if ((dateValue is double) && ((double)dateValue >= 0))
                    reminderInfo.AlertTime = DateTime.FromOADate((double)dateValue);
            }

            if (!string.IsNullOrEmpty(minutesColumn))
                reminderInfo.TimeBeforeStart = new TimeSpan(0, (int)selectReader[minutesColumn], 0);

            return new ReminderInfoCollectionContextElement(reminders, DateSavingType.LocalTime).ValueToString();
        }
    }
}
