﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Express2XtraScheduler.Core.ExpressScheduler
{
    enum EventType : byte
    {
        None = 0x00,
        Pattern = 0x01,
        Occurrence = 0x02,
        Exception = 0x03,
        Custom = 0x04
    };

    [Flags]
    enum EventOptions : byte
    {
        AllDayEvent = 0x01,
        Enabled = 0x02,
        Reminder = 0x04,
        Collapsed = 0x08,
        Group = 0x10
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    struct EventRecurrenceInfo
    {
        [Flags]
        public enum Days : byte
        {
            Sunday = 0x01,
            Monday = 0x02,
            Tuesday = 0x04,
            Wednesday = 0x08,
            Thursday = 0x10,
            Friday = 0x20,
            Saturday = 0x40
        }

        public enum RecurrenceKind : byte
        {
            Daily,
            Weekly,
            Monthly,
            Yearly
        }

        public enum DayTypeKind : byte
        {
            Day,
            EveryDay,
            WeekDay,
            WeekEndDay,
            Sunday,
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday
        }

        public int Count;
        public int DayNumber;
        public DayTypeKind DayType;
        public double Finish;
        public Days OccurDays;
        public int Periodicity;
        public RecurrenceKind Recurrence;
        public double Start;
        public int YearPeriodicity;
        public byte Reserved1;
        public int DismissDate;
    }
}
