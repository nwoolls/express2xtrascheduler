﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Express2XtraScheduler.Core
{
    class DataAccess
    {
        public static DbConnection GetConnection(string connectionString)
        {
            DbProviderFactory providerFactory = DbProviderFactories.GetFactory("System.Data.OleDb");

            DbConnection connection = providerFactory.CreateConnection();
            connection.ConnectionString = connectionString;

            return connection;
        }

        public static string GetSelectSqlString(string tableName, List<ColumnMapping> columnMappings, string keyColumn, object keyValue)
        {
            StringBuilder selectSql = new StringBuilder("SELECT ");

            //append columns to select
            foreach (ColumnMapping sourceMapping in columnMappings)
                if (!string.IsNullOrEmpty(sourceMapping.ColumnName))
                    selectSql.AppendFormat("[{0}],", sourceMapping.ColumnName);

            //remove trailing comma
            selectSql = selectSql.Remove(selectSql.Length - 1, 1);

            selectSql.AppendFormat(" FROM [{0}]", tableName);

            if (keyValue != null)
                selectSql.AppendFormat(" WHERE [{0}] = ?", keyColumn);

            selectSql.AppendFormat(" ORDER BY [{0}]", keyColumn);

            return selectSql.ToString();
        }

        public static DbCommand GetSelectCommand(DbConnection connection, string tableName, List<ColumnMapping> columnMappings, string keyColumn)
        {
            DbCommand selectCommand = connection.CreateCommand();

            selectCommand.CommandText = GetSelectSqlString(tableName, columnMappings, keyColumn, null);

            return selectCommand;
        }

        public static DbCommand GetSelectByKeyCommand(DbConnection connection, string tableName, List<ColumnMapping> columnMappings, string keyColumn, object keyValue)
        {
            DbCommand selectCommand = connection.CreateCommand();

            selectCommand.CommandText = GetSelectSqlString(tableName, columnMappings, keyColumn, keyValue);
            DbParameter keyParam = selectCommand.CreateParameter();
            keyParam.Value = keyValue;
            selectCommand.Parameters.Add(keyParam);

            return selectCommand;
        }

        public static DbCommand GetSelectCountCommand(DbConnection connection, string tableName)
        {
            DbCommand selectCommand = connection.CreateCommand();

            selectCommand.CommandText = string.Format("SELECT COUNT(*) FROM {0}", tableName);

            return selectCommand;
        }

        public static void SetIdentityInsert(DbConnection connection, string tableName, bool enabled)
        {
            using (DbCommand deleteCommand = connection.CreateCommand())
            {
                string sql = string.Format("SET IDENTITY_INSERT {0} {1}", tableName, enabled ? "ON" : "OFF");
                deleteCommand.CommandText = sql;
                deleteCommand.ExecuteNonQuery();
            }
        }

        public static void DeleteTableContents(DbConnection connection, string tableName)
        {
            using (DbCommand deleteCommand = connection.CreateCommand())
            {
                string deleteSql = string.Format("DELETE FROM [{0}]", tableName);
                deleteCommand.CommandText = deleteSql;
                deleteCommand.ExecuteNonQuery();
            }
        }

        public static string GetInsertSqlString(string tableName, Dictionary<string, object> columnValues)
        {
            StringBuilder insertSql = new StringBuilder(string.Format("INSERT INTO [{0}] (", tableName));

            //append column names for insert
            foreach (KeyValuePair<string, object> item in columnValues)
                insertSql.AppendFormat("[{0}],", item.Key);

            //remove trailing comma
            insertSql = insertSql.Remove(insertSql.Length - 1, 1);
            insertSql.Append(") VALUES (");

            //append parameters for insert
            foreach (KeyValuePair<string, object> item in columnValues)
                insertSql.Append("?,");

            //remove trailing comma
            insertSql = insertSql.Remove(insertSql.Length - 1, 1);
            insertSql.Append(")");

            return insertSql.ToString();
        }
    }
}
