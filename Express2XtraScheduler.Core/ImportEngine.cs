﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using DevExpress.XtraScheduler;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml;

namespace Express2XtraScheduler.Core
{
    public class ImportEngine : IDisposable
    {
        private DatabaseSettings expressSchedulerSettings;
        private Action<byte> progressCallback;
        private DatabaseSettings xtraSchedulerSettings;
        private DbConnection sourceConnection;
        private DbConnection destinationConnection;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (sourceConnection != null)
                {
                    sourceConnection.Dispose();
                    sourceConnection = null;
                }
                if (destinationConnection != null)
                {
                    destinationConnection.Dispose();
                    destinationConnection = null;
                }
            }
        }

        ~ImportEngine()
        {
            Dispose(false);
        }

        /// <summary>
        /// Imports data from ExpressScheduler format to XtraScheduler format. progressCallback is optional, and will be called with a
        /// value ranging from 0-100 for import progress.
        /// </summary>
        /// <param name="expressSchedulerSettings"></param>
        /// <param name="xtraSchedulerSettings"></param>
        /// <param name="progressCallback"></param>
        public void ImportSchedulerData(DatabaseSettings expressSchedulerSettings, DatabaseSettings xtraSchedulerSettings, Action<byte> progressCallback)
        {
            this.expressSchedulerSettings = expressSchedulerSettings;
            this.xtraSchedulerSettings = xtraSchedulerSettings;
            this.progressCallback = progressCallback;

            using (sourceConnection = DataAccess.GetConnection(expressSchedulerSettings.ConnectionString))
            using (destinationConnection = DataAccess.GetConnection(xtraSchedulerSettings.ConnectionString))
            {
                sourceConnection.Open();
                destinationConnection.Open();

                if (!string.IsNullOrEmpty(expressSchedulerSettings.ResourceTableName) && !string.IsNullOrEmpty(xtraSchedulerSettings.ResourceTableName))
                    ImportResources();

                if (!string.IsNullOrEmpty(expressSchedulerSettings.AppointmentTableName) && !string.IsNullOrEmpty(xtraSchedulerSettings.AppointmentTableName))
                    ImportAppointments();
            }
        }

        private void ImportAppointments()
        {
            string idColumn = ColumnNameForProperty("ID", expressSchedulerSettings.AppointmentColumnMappings);

            using (DbCommand selectCommand = DataAccess.GetSelectCommand(sourceConnection, expressSchedulerSettings.AppointmentTableName, expressSchedulerSettings.AppointmentColumnMappings, idColumn))
            {
                DataAccess.DeleteTableContents(destinationConnection, xtraSchedulerSettings.AppointmentTableName);
                DataAccess.SetIdentityInsert(destinationConnection, xtraSchedulerSettings.AppointmentTableName, true);
                try
                {
                    using (DbDataReader selectReader = selectCommand.ExecuteReader())
                    {
                        int totalAppointments = GetNumberOfAppointments();
                        int appointmentCounter = 1;
                        byte oldProgress = 0;

                        while (selectReader.Read())
                        {
                            Dictionary<string, object> columnValues = GetAppointmentRowColumnValues(selectReader);
                            ProcessImportRow(xtraSchedulerSettings.AppointmentTableName, columnValues);
                            if (progressCallback != null)
                            {
                                double progressDouble = ((double)appointmentCounter / (double)totalAppointments) * 100;
                                byte newProgress = Convert.ToByte(progressDouble);
                                if (oldProgress != newProgress)
                                {
                                    progressCallback(newProgress);
                                    oldProgress = newProgress;
                                }
                            }
                            appointmentCounter++;
                        }
                    }
                }
                finally
                {
                    DataAccess.SetIdentityInsert(destinationConnection, xtraSchedulerSettings.AppointmentTableName, false);
                }
            }
        }

        private int GetNumberOfAppointments()
        {
            using (DbCommand selectCommand = DataAccess.GetSelectCountCommand(sourceConnection, expressSchedulerSettings.AppointmentTableName))
            using (DbDataReader selectReader = selectCommand.ExecuteReader())
                if (selectReader.Read())
                    return (int)selectReader[0];

            return 0;
        }

        private Dictionary<string, object> GetAppointmentRowColumnValues(DbDataReader selectReader)
        {
            Dictionary<string, object> columnValues = new Dictionary<string, object>();

            AddRowColumnValue(selectReader, columnValues, "Caption", "Subject", x => x,
                expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "EventType", "Type", x =>
            {
                Express2XtraScheduler.Core.ExpressScheduler.EventType sourceEventType = (ExpressScheduler.EventType)(int)x;
                return sourceEventType.ToAppointmentType();
            }, expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "ID", "AppointmentId", x => x,
                expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "Start", "Start", x =>
            {
                //may be a DateTime or a double
                if (x is double)
                {
                    if ((double)x >= 0)
                        return DateTime.FromOADate((double)x);
                    else
                        return null;
                }
                else
                    return (DateTime)x;
            }, expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "Finish", "End", x =>
            {
                //may be a DateTime or a double
                if (x is double)
                {
                    if ((double)x >= 0)
                        return DateTime.FromOADate((double)x);
                    else
                        return null;
                }
                else
                    return (DateTime)x;
            }, expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "LabelColor", "Label", x =>
            {
                return ExpressSchedulerConversion.ColorToColorIndex((int)x);
            }, expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "Location", "Location", x => x,
                expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "Message", "Description", x => x,
                expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "Options", "AllDay", x =>
            {
                ExpressScheduler.EventOptions eventOptions = (ExpressScheduler.EventOptions)(int)x;
                return ((eventOptions & ExpressScheduler.EventOptions.AllDayEvent) == ExpressScheduler.EventOptions.AllDayEvent);
            }, expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            string columnName = XtraColumnNameForProperty("Type");
            //"Type" may not be mapped
            if (columnValues.ContainsKey(columnName))
            {
                if ((int)columnValues[columnName] != (int)AppointmentType.Normal)
                {
                    string sourceColumn = ExpressColumnNameForProperty("RecurrenceInfo");
                    string destinationColumn = XtraColumnNameForProperty("RecurrenceInfo");
                    string allDayColumn = XtraColumnNameForProperty("AllDay");
                    string startColumn = XtraColumnNameForProperty("Start");

                    if (!string.IsNullOrEmpty(sourceColumn) && !string.IsNullOrEmpty(destinationColumn))
                        columnValues[destinationColumn] = RecurrenceInfoToXML(selectReader, columnValues, sourceColumn, allDayColumn, startColumn);
                }
            }

            columnName = ExpressColumnNameForProperty("Options");
            //"Options" may not be mapped
            if (!string.IsNullOrEmpty(columnName))
            {
                object optionsValue = selectReader[columnName];
                if (optionsValue != DBNull.Value)
                {
                    if (((ExpressScheduler.EventOptions)(int)optionsValue & ExpressScheduler.EventOptions.Reminder) == ExpressScheduler.EventOptions.Reminder)
                    {
                        columnName = XtraColumnNameForProperty("ReminderInfo");
                        //"ReminderInfo" may not be mapped
                        if (!string.IsNullOrEmpty(columnName))
                        {
                            string dateColumn = ExpressColumnNameForProperty("ReminderDate");
                            string minutesColumn = ExpressColumnNameForProperty("ReminderMinutesBeforeStart");
                            columnValues[columnName] = ExpressSchedulerConversion.ReminderInfoToXML(selectReader, dateColumn, minutesColumn);
                        }
                    }
                }
            }

            AddRowColumnValue(selectReader, columnValues, "ResourceID", "ResourceId", x =>
            {
                string oldResourceID = null;

                //seen x as a string from an MDB or byte[] from an Advantage DB
                if (x is string)
                    oldResourceID = (string)x;
                else if (x is byte[])
                {
                    byte[] bytes = (byte[])x;

                    if (bytes.Length == 2)
                    {
                        //junk value - verified
                        return null;
                    }
                    else
                        oldResourceID = Encoding.Default.GetString(bytes);
                }

                return ExpressSchedulerConversion.ResourceIDBlobToXML(oldResourceID);
            }, expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "State", "Status", x => x,
                expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "TaskCompleteField", "PercentComplete", x => x,
                expressSchedulerSettings.AppointmentColumnMappings, xtraSchedulerSettings.AppointmentColumnMappings);

            return columnValues;
        }

        private string RecurrenceInfoToXML(DbDataReader selectReader, Dictionary<string, object> columnValues, string sourceColumn, string allDayColumn, string startColumn)
        {
            object recurObj = new ExpressScheduler.EventRecurrenceInfo();

            //may be either a string or byte[]
            object sourceValue = selectReader[sourceColumn];
            byte[] recurrenceBytes = null;
            if (sourceValue is string)
            {
                string recurrenceString = (string)sourceValue;
                recurrenceBytes = Encoding.Default.GetBytes(recurrenceString);
            }
            else if (sourceValue is byte[])
            {
                recurrenceBytes = (byte[])sourceValue;
            }

            if (recurrenceBytes == null)
                return null;

            recurrenceBytes.ToStructure(ref recurObj);
            ExpressScheduler.EventRecurrenceInfo sourceRecurrenceInfo = (ExpressScheduler.EventRecurrenceInfo)recurObj;

            RecurrenceInfo destinationRecurrenceInfo = new RecurrenceInfo();

            if (columnValues.ContainsKey(allDayColumn))
                destinationRecurrenceInfo.AllDay = (bool)columnValues[allDayColumn];

            destinationRecurrenceInfo.DayNumber = sourceRecurrenceInfo.DayNumber;

            destinationRecurrenceInfo.Periodicity = sourceRecurrenceInfo.Periodicity;

            if (sourceRecurrenceInfo.Start > 0)
                destinationRecurrenceInfo.Start = DateTime.FromOADate(sourceRecurrenceInfo.Start);
            else
            {
                //may be either a DateTime or a double
                object startValue = columnValues[startColumn];
                if (startValue is DateTime)
                    destinationRecurrenceInfo.Start = (DateTime)startValue;
                else if (startValue is double)
                    destinationRecurrenceInfo.Start = DateTime.FromOADate((double)startValue);
            }

            destinationRecurrenceInfo.Type = sourceRecurrenceInfo.Recurrence.ToRecurrenceType();

            WeekDays destinationWeekDays = sourceRecurrenceInfo.DayTypeToWeekDays();

            if (destinationWeekDays != 0)
                //check for 0 - XtraScheduler will not allow that for Daily recurrences
                destinationRecurrenceInfo.WeekDays = destinationWeekDays;

            //this must occur AFTER destinationRecurrenceInfo.Start is set - setting .Start changes .End
            destinationRecurrenceInfo.Range = RecurrenceRange.NoEndDate;
            if (sourceRecurrenceInfo.Count > 0)
            {
                destinationRecurrenceInfo.OccurrenceCount = sourceRecurrenceInfo.Count;
                destinationRecurrenceInfo.Range = RecurrenceRange.OccurrenceCount;
            }
            else if (sourceRecurrenceInfo.Finish > 0)
            {
                DateTime proposedEnd = DateTime.FromOADate(sourceRecurrenceInfo.Finish);

                if (proposedEnd > destinationRecurrenceInfo.Start)
                {
                    destinationRecurrenceInfo.End = proposedEnd;
                    destinationRecurrenceInfo.Range = RecurrenceRange.EndByDate;
                }
            }

            if (sourceRecurrenceInfo.Recurrence == ExpressScheduler.EventRecurrenceInfo.RecurrenceKind.Monthly)
                destinationRecurrenceInfo.WeekOfMonth = sourceRecurrenceInfo.DayInfoToWeekOfMonth();

            if (sourceRecurrenceInfo.Recurrence == ExpressScheduler.EventRecurrenceInfo.RecurrenceKind.Yearly)
            {
                destinationRecurrenceInfo.Month = destinationRecurrenceInfo.Periodicity;
                destinationRecurrenceInfo.Periodicity = 0;
                destinationRecurrenceInfo.WeekOfMonth = sourceRecurrenceInfo.DayInfoToWeekOfMonth();
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(destinationRecurrenceInfo.ToXml(DateSavingType.LocalTime));

            string parentIdColumn = ExpressColumnNameForProperty("ParentID");

            if (!string.IsNullOrEmpty(parentIdColumn) && (selectReader[parentIdColumn] != DBNull.Value))
            {
                //need to get the recurrence pattern
                Guid parentRecurrenceId = GetRecurrenceInfoIdForAppointment((int)selectReader[parentIdColumn]);
                if (parentRecurrenceId != Guid.Empty)
                    doc.DocumentElement.SetAttribute("Id", parentRecurrenceId.ToString());
            }

            string recurrenceIndexColumn = ExpressColumnNameForProperty("RecurrenceIndex");

            if (!string.IsNullOrEmpty(recurrenceIndexColumn))
                //need to get the recurrence pattern
                doc.DocumentElement.SetAttribute("Index", selectReader[recurrenceIndexColumn].ToString());

            return doc.InnerXml;
        }

        private Guid GetRecurrenceInfoIdForAppointment(int appointmentId)
        {
            string appointmentIdColumn = XtraColumnNameForProperty("AppointmentId");

            DbCommand selectCommand = DataAccess.GetSelectByKeyCommand(destinationConnection, xtraSchedulerSettings.AppointmentTableName, xtraSchedulerSettings.AppointmentColumnMappings, appointmentIdColumn, appointmentId);

            using (DbDataReader selectReader = selectCommand.ExecuteReader())
            {
                if (selectReader.Read())
                {
                    XmlDocument doc = new XmlDocument();
                    object sourceValue = selectReader[XtraColumnNameForProperty("RecurrenceInfo")];
                    if (sourceValue != DBNull.Value)
                    {
                        doc.LoadXml(sourceValue.ToString());
                        return Guid.Parse(doc.DocumentElement.GetAttribute("Id"));
                    }
                }
            }

            return Guid.Empty;
        }
        
        private void ImportResources()
        {
            string idColumn = ColumnNameForProperty("ResourceID", expressSchedulerSettings.AppointmentColumnMappings);

            using (DbCommand selectCommand = DataAccess.GetSelectCommand(sourceConnection, expressSchedulerSettings.ResourceTableName, expressSchedulerSettings.ResourceColumnMappings, idColumn))
            {
                DataAccess.DeleteTableContents(destinationConnection, xtraSchedulerSettings.ResourceTableName);
                DataAccess.SetIdentityInsert(destinationConnection, xtraSchedulerSettings.ResourceTableName, true);
                try
                {
                    using (DbDataReader selectReader = selectCommand.ExecuteReader())
                    {
                        while (selectReader.Read())
                        {
                            Dictionary<string, object> columnValues = GetResourceRowColumnValues(selectReader);
                            ProcessImportRow(xtraSchedulerSettings.ResourceTableName, columnValues);
                        }
                    }
                }
                finally
                {
                    DataAccess.SetIdentityInsert(destinationConnection, xtraSchedulerSettings.ResourceTableName, false);
                }
            }
        }

        private Dictionary<string, object> GetResourceRowColumnValues(DbDataReader selectReader)
        {
            Dictionary<string, object> columnValues = new Dictionary<string, object>();

            AddRowColumnValue(selectReader, columnValues, "ResourceColor", "Color", x =>
            {
                return ColorTranslator.ToOle(ExpressSchedulerConversion.DelphiColorToColor((uint)(int)x));
            }, expressSchedulerSettings.ResourceColumnMappings, xtraSchedulerSettings.ResourceColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "ResourceID", "Id", x => x, expressSchedulerSettings.ResourceColumnMappings, xtraSchedulerSettings.ResourceColumnMappings);

            AddRowColumnValue(selectReader, columnValues, "ResourceName", "Caption", x => x, expressSchedulerSettings.ResourceColumnMappings, xtraSchedulerSettings.ResourceColumnMappings);

            return columnValues;
        }

        private void ProcessImportRow(string destinationTableName, Dictionary<string, object> columnValues)
        {
            DbCommand insertCommand = destinationConnection.CreateCommand();

            insertCommand.CommandText = DataAccess.GetInsertSqlString(destinationTableName, columnValues);

            //add parameters and values
            foreach (KeyValuePair<string, object> item in columnValues)
            {
                DbParameter insertParameter = insertCommand.CreateParameter();
                insertParameter.Value = item.Value;
                insertCommand.Parameters.Add(insertParameter);
            }

            insertCommand.ExecuteNonQuery();
        }

        private static void AddRowColumnValue(DbDataReader selectReader, Dictionary<string, object> columnValues,
            string sourceProperty, string destinationProperty, Func<object, object> conversion,
            List<ColumnMapping> sourceColumnMappings, List<ColumnMapping> destinationColumnMappings)
        {
            string sourceColumn = ColumnNameForProperty(sourceProperty, sourceColumnMappings);
            string destinationColumn = ColumnNameForProperty(destinationProperty, destinationColumnMappings);

            if (!string.IsNullOrEmpty(sourceColumn) && !string.IsNullOrEmpty(destinationColumn))
            {
                object sourceValue = selectReader[sourceColumn];
                if (sourceValue != DBNull.Value)
                    columnValues[destinationColumn] = conversion(sourceValue);
            }
        }

        private string ExpressColumnNameForProperty(string propertyName)
        {
            return ColumnNameForProperty(propertyName, expressSchedulerSettings.AppointmentColumnMappings);
        }

        private string XtraColumnNameForProperty(string propertyName)
        {
            return ColumnNameForProperty(propertyName, xtraSchedulerSettings.AppointmentColumnMappings);
        }

        private static string ColumnNameForProperty(string propertyName, List<ColumnMapping> columnMappings)
        {
            string columnName = (from m in columnMappings
                                 where m.PropertyName == propertyName
                                 select m.ColumnName).FirstOrDefault();
            return columnName;
        }
    }
}
