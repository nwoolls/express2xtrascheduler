﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Data.OleDb;
using Express2XtraScheduler.Core;

namespace Express2XtraScheduler.UI
{
    public partial class DatabaseSetupControl : UserControl
    {
        public DatabaseSetupControl()
        {
            InitializeComponent();

            SetupColumnMappings();
        }

        private void SetupColumnMappings()
        {
            SetupAppointmentColumnMappings();
            SetupResourceColumnMappings();
        }

        private SchedulerKind schedulerKind;
        public SchedulerKind SchedulerKind
        {
            get
            {
                return schedulerKind;
            }
            set
            {
                schedulerKind = value;
                SetupColumnMappings();
            }
        }

        private List<ColumnMapping> appointmentColumnMappings;
        private List<ColumnMapping> resourceColumnMappings;

        public void LoadFromSettings(DatabaseSettings databaseSettings)
        {
            connectionStringEdit.Text = databaseSettings.ConnectionString;

            if (!string.IsNullOrEmpty(connectionStringEdit.Text))
                PopulateTables();
            
            appointmentTableCombo.Text = databaseSettings.AppointmentTableName;
            resourceTableCombo.Text = databaseSettings.ResourceTableName;

            if (!string.IsNullOrEmpty(connectionStringEdit.Text) && !string.IsNullOrEmpty(appointmentTableCombo.Text))
                PopulateAppointmentColumns();

            if (!string.IsNullOrEmpty(connectionStringEdit.Text) && !string.IsNullOrEmpty(resourceTableCombo.Text))
                PopulateResourceColumns();

            LoadColumnMappings(databaseSettings.AppointmentColumnMappings, appointmentColumnMappings);
            LoadColumnMappings(databaseSettings.ResourceColumnMappings, resourceColumnMappings);
        }

        private static void LoadColumnMappings(List<ColumnMapping> sourceColumnMappings, List<ColumnMapping> destinationColumnMappings)
        {
            foreach (ColumnMapping columnMapping in sourceColumnMappings)
            {
                ColumnMapping existingMapping = (from c in destinationColumnMappings
                                                 where c.PropertyName == columnMapping.PropertyName
                                                 select c).FirstOrDefault();
                if (existingMapping != null)
                    existingMapping.ColumnName = columnMapping.ColumnName;
            }
        }

        public void SaveToSettings(DatabaseSettings databaseSettings)
        {
            databaseSettings.ConnectionString = connectionStringEdit.Text;
            databaseSettings.AppointmentTableName = appointmentTableCombo.Text;
            databaseSettings.ResourceTableName = resourceTableCombo.Text;
            databaseSettings.AppointmentColumnMappings = appointmentColumnMappings;
            databaseSettings.ResourceColumnMappings = resourceColumnMappings;
        }

        private void SetupAppointmentColumnMappings()
        {
            appointmentColumnMappings = new List<ColumnMapping>();

            if (SchedulerKind == SchedulerKind.XtraScheduler)
            {
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "AllDay" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "AppointmentId" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Description" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "End" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Label" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Location" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "PercentComplete" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceInfo" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderInfo" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceId" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Start" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Status" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Subject" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Type" });
            }
            else
            {
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Caption" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "EventType" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Finish" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ID" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "LabelColor" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Location" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Message" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Options" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ParentID" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceIndex" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceInfo" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderDate" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderMinutesBeforeStart" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceID" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Start" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "State" });
                appointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "TaskCompleteField" });
            }

            appointmentMappingBindingSource.DataSource = appointmentColumnMappings;
        }

        private void SetupResourceColumnMappings()
        {
            resourceColumnMappings = new List<ColumnMapping>();

            if (SchedulerKind == SchedulerKind.XtraScheduler)
            {
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Caption" });
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Color" });
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Id" });
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Image" });
            }
            else
            {
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceColor" });
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceID" });
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceImageIndex" });
                resourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceName" });
            }

            resourceMappingBindingSource.DataSource = resourceColumnMappings;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            connectionStringEdit.Text = BuildConnectionString(connectionStringEdit.Text);
            connectionStringEdit.Focus();
        }

        private static string BuildConnectionString(string currentString)
        {
            string newConnectionString = "";

            MSDASC.DataLinks dataLinks = new MSDASC.DataLinks();
            ADODB.Connection connection;
            if (!string.IsNullOrEmpty(currentString))
            {
                connection = new ADODB.Connection();
                connection.ConnectionString = currentString;
                object refConnection = connection;
                dataLinks.PromptEdit(ref refConnection);
                newConnectionString = connection.ConnectionString;
            }
            else
            {
                connection = dataLinks.PromptNew();
                if (connection != null)
                    newConnectionString = ((ADODB.Connection)connection).ConnectionString;
            }

            return newConnectionString;
        }

        private void PopulateAppointmentColumns()
        {
            List<string> appointmentColumnNames = new List<string>();

            if (!string.IsNullOrEmpty(appointmentTableCombo.Text))
                PopulateColumnNames(appointmentColumnNames, appointmentTableCombo.Text);

            UpdateMappingsForColumnNames(appointmentColumnMappings, appointmentColumnNames);

            BindingSource columnsSource = new BindingSource() { DataSource = appointmentColumnNames };
            appointmentColumnNameColumn.DataSource = columnsSource;
        }

        private void PopulateResourceColumns()
        {
            List<string> resourceColumnNames = new List<string>();

            if (!string.IsNullOrEmpty(resourceTableCombo.Text))
                PopulateColumnNames(resourceColumnNames, resourceTableCombo.Text);

            UpdateMappingsForColumnNames(resourceColumnMappings, resourceColumnNames);
            
            BindingSource columnsSource = new BindingSource() { DataSource = resourceColumnNames };
            resourceColumnNameColumn.DataSource = columnsSource;
        }

        private static void UpdateMappingsForColumnNames(List<ColumnMapping> columnMappings, List<string> columnNames)
        {
            foreach (ColumnMapping mapping in columnMappings)
            {
                if (!columnNames.Contains(mapping.ColumnName))
                    mapping.ColumnName = "";

                if (string.IsNullOrEmpty(mapping.ColumnName))
                {
                    string foundColumn = columnNames.Find(s => s.Equals(mapping.PropertyName, StringComparison.CurrentCultureIgnoreCase));
                    if (!string.IsNullOrEmpty(foundColumn))
                        mapping.ColumnName = foundColumn;
                }
            }
        }

        private void PopulateColumnNames(List<string> columnNames, string tableName)
        {
            using (DataTable schemaTable = GetSchemaTable(connectionStringEdit.Text, "Columns"))
            {
                string rowFilter = string.Format("TABLE_NAME = '{0}'", tableName);
                DataView filteredView = schemaTable.DefaultView;
                filteredView.RowFilter = rowFilter;
                filteredView.Sort = "COLUMN_NAME";

                foreach (DataRowView dataRowView in filteredView)
                    columnNames.Add((string)dataRowView.Row["COLUMN_NAME"]);
            }
        }

        public static DataTable GetSchemaTable(string connectionString, string collectionName)
        {
            DataTable schemaTable;

            using (OleDbConnection connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                schemaTable = connection.GetSchema(collectionName);
                connection.Close();
            }
            return schemaTable;
        }

        private void PopulateTables()
        {
            List<string> tableNames = new List<string>();

            string appointmentTableName = appointmentTableCombo.Text;
            string resourceTableName = resourceTableCombo.Text;
            try
            {
                using (DataTable schemaTable = GetSchemaTable(connectionStringEdit.Text, "Tables"))
                {
                    const string rowFilter = "TABLE_TYPE = 'TABLE'";
                    DataView filteredView = schemaTable.DefaultView;
                    filteredView.RowFilter = rowFilter;

                    foreach (DataRowView dataRowView in filteredView)
                        tableNames.Add((string)dataRowView.Row["TABLE_NAME"]);

                    BindingSource tablesSource = new BindingSource() { DataSource = tableNames };

                    appointmentTableCombo.DataSource = tablesSource;

                    BindingSource otherSource = new BindingSource() { DataSource = tableNames };
                    
                    resourceTableCombo.DataSource = otherSource;

                    //don't automatically select the first item in the list
                    appointmentTableCombo.Text = "";
                    resourceTableCombo.Text = "";
                }
            }
            finally
            {
                string foundTable = tableNames.Find(s => s.Equals(appointmentTableName, StringComparison.CurrentCultureIgnoreCase));
                if (!string.IsNullOrEmpty(foundTable))
                    appointmentTableCombo.Text = foundTable;

                foundTable = tableNames.Find(s => s.Equals(resourceTableName, StringComparison.CurrentCultureIgnoreCase));
                if (!string.IsNullOrEmpty(foundTable))
                    resourceTableCombo.Text = foundTable;
            }
        }
        
        private void resourceTableCombo_Validated(object sender, EventArgs e)
        {
            PopulateResourceColumns();
        }

        private void appointmentTableCombo_Validated(object sender, EventArgs e)
        {
            PopulateAppointmentColumns();
        }

        private void connectionStringEdit_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(connectionStringEdit.Text))
                PopulateTables();
        }
    }
}
