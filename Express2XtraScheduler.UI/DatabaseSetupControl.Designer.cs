﻿namespace Express2XtraScheduler.UI
{
    partial class DatabaseSetupControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.connectionStringEdit = new System.Windows.Forms.TextBox();
            this.mappingsGridView = new System.Windows.Forms.DataGridView();
            this.resourcePropertyNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resourceColumnNameColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.resourceMappingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.resourceTableCombo = new System.Windows.Forms.ComboBox();
            this.appointmentTableCombo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.appointmentPropertyNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.appointmentColumnNameColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.appointmentMappingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mappingsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourceMappingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentMappingBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Resources table:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(514, 79);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Build";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Connection string:";
            // 
            // connectionStringEdit
            // 
            this.connectionStringEdit.Location = new System.Drawing.Point(109, 17);
            this.connectionStringEdit.Multiline = true;
            this.connectionStringEdit.Name = "connectionStringEdit";
            this.connectionStringEdit.Size = new System.Drawing.Size(480, 56);
            this.connectionStringEdit.TabIndex = 0;
            this.connectionStringEdit.Validated += new System.EventHandler(this.connectionStringEdit_Validated);
            // 
            // mappingsGridView
            // 
            this.mappingsGridView.AllowUserToAddRows = false;
            this.mappingsGridView.AllowUserToDeleteRows = false;
            this.mappingsGridView.AllowUserToResizeRows = false;
            this.mappingsGridView.AutoGenerateColumns = false;
            this.mappingsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mappingsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.resourcePropertyNameColumn,
            this.resourceColumnNameColumn});
            this.mappingsGridView.DataSource = this.resourceMappingBindingSource;
            this.mappingsGridView.Location = new System.Drawing.Point(14, 164);
            this.mappingsGridView.MultiSelect = false;
            this.mappingsGridView.Name = "mappingsGridView";
            this.mappingsGridView.RowHeadersVisible = false;
            this.mappingsGridView.Size = new System.Drawing.Size(278, 193);
            this.mappingsGridView.TabIndex = 4;
            // 
            // resourcePropertyNameColumn
            // 
            this.resourcePropertyNameColumn.DataPropertyName = "PropertyName";
            this.resourcePropertyNameColumn.HeaderText = "Property Name";
            this.resourcePropertyNameColumn.Name = "resourcePropertyNameColumn";
            this.resourcePropertyNameColumn.ReadOnly = true;
            this.resourcePropertyNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.resourcePropertyNameColumn.Width = 125;
            // 
            // resourceColumnNameColumn
            // 
            this.resourceColumnNameColumn.DataPropertyName = "ColumnName";
            this.resourceColumnNameColumn.HeaderText = "Column Name";
            this.resourceColumnNameColumn.Name = "resourceColumnNameColumn";
            this.resourceColumnNameColumn.Width = 125;
            // 
            // resourceMappingBindingSource
            // 
            this.resourceMappingBindingSource.DataSource = typeof(Express2XtraScheduler.Core.ColumnMapping);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Mapping";
            // 
            // resourceTableCombo
            // 
            this.resourceTableCombo.FormattingEnabled = true;
            this.resourceTableCombo.Location = new System.Drawing.Point(109, 115);
            this.resourceTableCombo.Name = "resourceTableCombo";
            this.resourceTableCombo.Size = new System.Drawing.Size(183, 21);
            this.resourceTableCombo.TabIndex = 2;
            this.resourceTableCombo.Validated += new System.EventHandler(this.resourceTableCombo_Validated);
            // 
            // appointmentTableCombo
            // 
            this.appointmentTableCombo.FormattingEnabled = true;
            this.appointmentTableCombo.Location = new System.Drawing.Point(414, 115);
            this.appointmentTableCombo.Name = "appointmentTableCombo";
            this.appointmentTableCombo.Size = new System.Drawing.Size(175, 21);
            this.appointmentTableCombo.TabIndex = 3;
            this.appointmentTableCombo.Validated += new System.EventHandler(this.appointmentTableCombo_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(308, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Appointments table:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.appointmentPropertyNameColumn,
            this.appointmentColumnNameColumn});
            this.dataGridView1.DataSource = this.appointmentMappingBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(311, 164);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(278, 193);
            this.dataGridView1.TabIndex = 5;
            // 
            // appointmentPropertyNameColumn
            // 
            this.appointmentPropertyNameColumn.DataPropertyName = "PropertyName";
            this.appointmentPropertyNameColumn.HeaderText = "Property Name";
            this.appointmentPropertyNameColumn.Name = "appointmentPropertyNameColumn";
            this.appointmentPropertyNameColumn.ReadOnly = true;
            this.appointmentPropertyNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.appointmentPropertyNameColumn.Width = 125;
            // 
            // appointmentColumnNameColumn
            // 
            this.appointmentColumnNameColumn.DataPropertyName = "ColumnName";
            this.appointmentColumnNameColumn.HeaderText = "Column Name";
            this.appointmentColumnNameColumn.Name = "appointmentColumnNameColumn";
            this.appointmentColumnNameColumn.Width = 125;
            // 
            // appointmentMappingBindingSource
            // 
            this.appointmentMappingBindingSource.DataSource = typeof(Express2XtraScheduler.Core.ColumnMapping);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(308, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Mapping";
            // 
            // DatabaseSetupControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.appointmentTableCombo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.resourceTableCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mappingsGridView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.connectionStringEdit);
            this.Name = "DatabaseSetupControl";
            this.Size = new System.Drawing.Size(631, 483);
            ((System.ComponentModel.ISupportInitialize)(this.mappingsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourceMappingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentMappingBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox connectionStringEdit;
        private System.Windows.Forms.DataGridView mappingsGridView;
        private System.Windows.Forms.BindingSource appointmentMappingBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox resourceTableCombo;
        private System.Windows.Forms.ComboBox appointmentTableCombo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.BindingSource resourceMappingBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn resourcePropertyNameColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn resourceColumnNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn appointmentPropertyNameColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn appointmentColumnNameColumn;
    }
}
