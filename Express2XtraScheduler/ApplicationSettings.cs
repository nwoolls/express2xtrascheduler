﻿using Express2XtraScheduler.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using Express2XtraScheduler.Core;

namespace Express2XtraScheduler
{
    public class ApplicationSettings
    {
        public ApplicationSettings()
        {
            ExpressSchedulerSettings = new DatabaseSettings();
            XtraSchedulerSettings = new DatabaseSettings();
        }

        public DatabaseSettings ExpressSchedulerSettings { get; set; }
        public DatabaseSettings XtraSchedulerSettings { get; set; }
    }
}
