﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using Express2XtraScheduler.Core;

namespace Express2XtraScheduler
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private ApplicationSettings applicationSettings;

        private void MainForm_Load(object sender, EventArgs e)
        {
            mainTabControl.TabIndex = 0;

            LoadSettings();
        }

        private void beginButton_Click(object sender, EventArgs e)
        {
            SaveSettings();

            using (ImportEngine engine = new ImportEngine())
            {
                engine.ImportSchedulerData(applicationSettings.ExpressSchedulerSettings, applicationSettings.XtraSchedulerSettings, ProgressCallback);
            }

            MessageBox.Show("Success!");
        }

        private void ProgressCallback(byte percentComplete)
        {
            progressBar1.Value = percentComplete;
        }
                
        private void LoadSettings()
        {
            LoadSettingsFromXML();

            expressSchedulerSetupControl.LoadFromSettings(applicationSettings.ExpressSchedulerSettings);
            xtraSchedulerSetupControl.LoadFromSettings(applicationSettings.XtraSchedulerSettings);
        }

        private void LoadSettingsFromXML()
        {
            string applicationSettingsFile = ApplicationSettingsFile();
            if (File.Exists(applicationSettingsFile))
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(ApplicationSettings));
                TextReader textReader = new StreamReader(applicationSettingsFile);
                try
                {
                    applicationSettings = (ApplicationSettings)deserializer.Deserialize(textReader);
                }
                finally
                {
                    textReader.Close();
                }
            }
            else
            {
                applicationSettings = new ApplicationSettings();
            }
        }

        private void SaveSettings()
        {
            expressSchedulerSetupControl.SaveToSettings(applicationSettings.ExpressSchedulerSettings);
            xtraSchedulerSetupControl.SaveToSettings(applicationSettings.XtraSchedulerSettings);

            SaveSettingsToXML();
        }

        private void SaveSettingsToXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ApplicationSettings));
            string applicationSettingsFile = ApplicationSettingsFile();
            Directory.CreateDirectory(Path.GetDirectoryName(applicationSettingsFile));
            using (TextWriter textWriter = new StreamWriter(ApplicationSettingsFile()))
            {
                try
                {
                    serializer.Serialize(textWriter, applicationSettings);
                }
                finally
                {
                    textWriter.Close();
                }
            }
        }

        private static string ApplicationSettingsFile()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name,
                "ApplicationSettings.xml"); ;
        }
    }
}
