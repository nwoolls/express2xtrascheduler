﻿namespace Express2XtraScheduler
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.expressSchedulerSetupControl = new Express2XtraScheduler.UI.DatabaseSetupControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.xtraSchedulerSetupControl = new Express2XtraScheduler.UI.DatabaseSetupControl();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.beginButton = new System.Windows.Forms.Button();
            this.mainTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.tabPage1);
            this.mainTabControl.Controls.Add(this.tabPage2);
            this.mainTabControl.Location = new System.Drawing.Point(12, 12);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(619, 411);
            this.mainTabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.expressSchedulerSetupControl);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(611, 385);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Source";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // expressSchedulerSetupControl
            // 
            this.expressSchedulerSetupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.expressSchedulerSetupControl.Location = new System.Drawing.Point(3, 3);
            this.expressSchedulerSetupControl.Name = "expressSchedulerSetupControl";
            this.expressSchedulerSetupControl.SchedulerKind = Express2XtraScheduler.UI.SchedulerKind.ExpressScheduler;
            this.expressSchedulerSetupControl.Size = new System.Drawing.Size(605, 379);
            this.expressSchedulerSetupControl.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.xtraSchedulerSetupControl);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(611, 385);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Destination";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // xtraSchedulerSetupControl
            // 
            this.xtraSchedulerSetupControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraSchedulerSetupControl.Location = new System.Drawing.Point(3, 3);
            this.xtraSchedulerSetupControl.Name = "xtraSchedulerSetupControl";
            this.xtraSchedulerSetupControl.SchedulerKind = Express2XtraScheduler.UI.SchedulerKind.XtraScheduler;
            this.xtraSchedulerSetupControl.Size = new System.Drawing.Size(605, 379);
            this.xtraSchedulerSetupControl.TabIndex = 1;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(13, 430);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(530, 23);
            this.progressBar1.TabIndex = 1;
            // 
            // beginButton
            // 
            this.beginButton.Location = new System.Drawing.Point(549, 430);
            this.beginButton.Name = "beginButton";
            this.beginButton.Size = new System.Drawing.Size(75, 23);
            this.beginButton.TabIndex = 2;
            this.beginButton.Text = "Begin";
            this.beginButton.UseVisualStyleBackColor = true;
            this.beginButton.Click += new System.EventHandler(this.beginButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 465);
            this.Controls.Add(this.beginButton);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.mainTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Convert ExpressScheduler Data to XtraScheduler";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private Express2XtraScheduler.UI.DatabaseSetupControl expressSchedulerSetupControl;
        private Express2XtraScheduler.UI.DatabaseSetupControl xtraSchedulerSetupControl;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button beginButton;


    }
}

