object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Express Scheduler App'
  ClientHeight = 334
  ClientWidth = 621
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxScheduler1: TcxScheduler
    Left = 0
    Top = 0
    Width = 621
    Height = 334
    ViewDay.Active = True
    Align = alClient
    EventOperations.SharingBetweenResources = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Storage = cxSchedulerDBStorage1
    TabOrder = 0
    Splitters = {
      DD0100007E0000006C02000083000000D801000001000000DD0100004D010000}
    StoredClientBounds = {01000000010000006C0200004D010000}
  end
  object cxSchedulerDBStorage1: TcxSchedulerDBStorage
    Resources.Items = <>
    Resources.DataSource = DataSource1
    Resources.ResourceColor = 'ResourceColor'
    Resources.ResourceID = 'ResourceID'
    Resources.ResourceImageIndex = 'ResourceImageIndex'
    Resources.ResourceName = 'ResourceName'
    CustomFields = <>
    DataSource = DataSource2
    FieldNames.ActualFinish = 'ActualFinish'
    FieldNames.ActualStart = 'ActualStart'
    FieldNames.Caption = 'Caption'
    FieldNames.EventType = 'EventType'
    FieldNames.Finish = 'Finish'
    FieldNames.ID = 'ID'
    FieldNames.LabelColor = 'LabelColor'
    FieldNames.Location = 'Location'
    FieldNames.Message = 'Message'
    FieldNames.Options = 'Options'
    FieldNames.ParentID = 'ParentID'
    FieldNames.RecurrenceIndex = 'RecurrenceIndex'
    FieldNames.RecurrenceInfo = 'RecurrenceInfo'
    FieldNames.ReminderDate = 'ReminderDate'
    FieldNames.ReminderMinutesBeforeStart = 'ReminderMinutesBeforeStart'
    FieldNames.ReminderResourcesData = 'ReminderResourcesData'
    FieldNames.ResourceID = 'ResourceID'
    FieldNames.Start = 'Start'
    FieldNames.State = 'State'
    FieldNames.TaskCompleteField = 'TaskCompleteField'
    FieldNames.TaskIndexField = 'TaskIndexField'
    FieldNames.TaskLinksField = 'TaskLinksField'
    FieldNames.TaskStatusField = 'TaskStatusField'
    Left = 448
    Top = 232
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Resources'
    Left = 168
    Top = 232
  end
  object ADOTable2: TADOTable
    Connection = ADOConnection1
    CursorType = ctStatic
    TableName = 'Appointments'
    Left = 240
    Top = 232
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Password="";User ID=Admin;Data ' +
      'Source=schedule.mdb;Mode=Share Deny None;Extended Properties="";' +
      'Jet OLEDB:System database="";Jet OLEDB:Registry Path="";Jet OLED' +
      'B:Database Password="";Jet OLEDB:Engine Type=5;Jet OLEDB:Databas' +
      'e Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:G' +
      'lobal Bulk Transactions=1;Jet OLEDB:New Database Password="";Jet' +
      ' OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=F' +
      'alse;Jet OLEDB:Don'#39't Copy Locale on Compact=False;Jet OLEDB:Comp' +
      'act Without Replica Repair=False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 328
    Top = 232
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 168
    Top = 288
  end
  object DataSource2: TDataSource
    DataSet = ADOTable2
    Left = 240
    Top = 288
  end
end
