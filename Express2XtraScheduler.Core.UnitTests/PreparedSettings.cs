﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Express2XtraScheduler.Core.UnitTests
{
    class PreparedSettings
    {
        public static void SetupAdsSourceSettings(DatabaseSettings adsSourceSettings)
        {
            adsSourceSettings.ConnectionString = @"Provider=Advantage OLE DB Provider;User ID=AdsSys;Password=eL33B41qPu;Data Source=..\..\Data\Advantage\CX.add;Advantage Server Type=ADS_LOCAL_SERVER;";

            adsSourceSettings.ResourceTableName = "";
            adsSourceSettings.AppointmentTableName = "CXS01";

            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Caption", ColumnName = "CAPTION" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "EventType", ColumnName = "TYPE" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Finish", ColumnName = "FINISH" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ID", ColumnName = "ID" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "LabelColor", ColumnName = "LABELCOLOR" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Location", ColumnName = "LOCATION" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Message", ColumnName = "MESSAGE" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Options", ColumnName = "OPTIONS" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ParentID", ColumnName = "PARENTID" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceIndex", ColumnName = "RECUR_IND" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceInfo", ColumnName = "RECUR_INFO" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderDate", ColumnName = "REMINDDATE" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderMinutesBeforeStart", ColumnName = "REMINDMIN" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceID", ColumnName = "RESOURCEID" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Start", ColumnName = "START" });
            adsSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "State", ColumnName = "STATE" });
        }

        public static void SetupJetSourceSettings(DatabaseSettings jetSourceSettings)
        {
            jetSourceSettings.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=..\..\Data\Access\schedule.mdb;Persist Security Info=False";

            jetSourceSettings.ResourceTableName = "Resources";
            jetSourceSettings.AppointmentTableName = "Appointments";

            jetSourceSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceColor", ColumnName = "ResourceColor" });
            jetSourceSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceID", ColumnName = "ResourceID" });
            jetSourceSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceImageIndex", ColumnName = "ResourceImageIndex" });
            jetSourceSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceName", ColumnName = "ResourceName" });

            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Caption", ColumnName = "Caption" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "EventType", ColumnName = "EventType" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Finish", ColumnName = "Finish" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ID", ColumnName = "ID" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "LabelColor", ColumnName = "LabelColor" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Location", ColumnName = "Location" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Message", ColumnName = "Message" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Options", ColumnName = "Options" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ParentID", ColumnName = "ParentID" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceIndex", ColumnName = "RecurrenceIndex" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceInfo", ColumnName = "RecurrenceInfo" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderDate", ColumnName = "ReminderDate" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderMinutesBeforeStart", ColumnName = "ReminderMinutesBeforeStart" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceID", ColumnName = "ResourceID" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Start", ColumnName = "Start" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "State", ColumnName = "State" });
            jetSourceSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "TaskCompleteField", ColumnName = "TaskCompleteField" });
        }

        public static void SetupLocalDBDestinationSettings(DatabaseSettings localDBDestinationSettings, string connectionString)
        {
            localDBDestinationSettings.ConnectionString = connectionString;

            localDBDestinationSettings.ResourceTableName = "Resources";
            localDBDestinationSettings.AppointmentTableName = "Appointments";

            localDBDestinationSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Caption", ColumnName = "ResourceName" });
            localDBDestinationSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Color", ColumnName = "Color" });
            localDBDestinationSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Id", ColumnName = "UniqueID" });
            localDBDestinationSettings.ResourceColumnMappings.Add(new ColumnMapping() { PropertyName = "Image", ColumnName = "Image" });

            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "AllDay", ColumnName = "AllDay" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "AppointmentId", ColumnName = "UniqueID" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Description", ColumnName = "Description" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "End", ColumnName = "EndDate" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Label", ColumnName = "Label" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Location", ColumnName = "Location" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "RecurrenceInfo", ColumnName = "RecurrenceInfo" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ReminderInfo", ColumnName = "ReminderInfo" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "ResourceId", ColumnName = "ResourceIDs" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Start", ColumnName = "StartDate" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Status", ColumnName = "Status" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Subject", ColumnName = "Subject" });
            localDBDestinationSettings.AppointmentColumnMappings.Add(new ColumnMapping() { PropertyName = "Type", ColumnName = "Type" });
        }
    }
}
