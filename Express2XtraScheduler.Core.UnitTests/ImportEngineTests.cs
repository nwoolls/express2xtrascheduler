﻿/* Copyright (C) 2012 Nathanial Woolls
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial 
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Common;
using System.Linq;
using System.Text.RegularExpressions;

namespace Express2XtraScheduler.Core.UnitTests
{
    [TestClass]
    public class ImportEngineTests
    {
        private static DatabaseSettings adsSourceSettings;
        private static DatabaseSettings jetSourceSettings;
        private static DatabaseSettings localDBDestinationSettings;
        private static string destinationConnectionString;

        private const string XtraSchedulerSchemaDDL = 
            @"CREATE TABLE [dbo].[Appointments] (
                    [UniqueID] [int] IDENTITY (1, 1) NOT NULL ,
                    [Type] [int] NULL ,
                    [StartDate] [smalldatetime] NULL ,
                    [EndDate] [smalldatetime] NULL ,
                    [AllDay] [bit] NULL ,
                    [Subject] [nvarchar] (50) NULL ,
                    [Location] [nvarchar] (50) NULL ,
                    [Description] [nvarchar](max) NULL ,
                    [Status] [int] NULL ,
                    [Label] [int] NULL ,
                    [ResourceID] [int] NULL ,
                    [ResourceIDs] [nvarchar](max) NULL ,
                    [ReminderInfo] [nvarchar](max) NULL ,
                    [RecurrenceInfo] [nvarchar](max) NULL ,
                    [CustomField1] [nvarchar](max) NULL
            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
            
            CREATE TABLE [dbo].[Resources] (
                    [UniqueID] [int] IDENTITY (1, 1) NOT NULL ,
                    [ResourceName] [nvarchar] (50) NULL ,
                    [Color] [int] NULL ,
                    [Image] [image] NULL ,
                    [CustomField1] [nvarchar](max) NULL
            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
            
            ALTER TABLE [dbo].[Appointments] WITH NOCHECK ADD
                    CONSTRAINT [PK_Appointments] PRIMARY KEY CLUSTERED
                    (
                            [UniqueID]
                    ) ON [PRIMARY];
            
            ALTER TABLE [dbo].[Resources] WITH NOCHECK ADD
                    CONSTRAINT [PK_Resources] PRIMARY KEY CLUSTERED
                    (
                            [UniqueID]
                    ) ON [PRIMARY];";
        private const string LocalDBDatabaseName = "Express2XtraSchedulerTests";

        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            //run once, then all tests are run
            //this is NOT run once-per-test

            //passing in true as the second param will delete and re-create the DB
            destinationConnectionString = LocalDB.GetLocalDBConnectionString(LocalDBDatabaseName, true);
            CreateLocalDBSchema();

            SetupLocalDBDestinationSettings();            
            SetupJetSourceSettings();
            SetupAdsSourceSettings();
        }

        [ClassCleanup()]
        public static void ClassCleanup()
        {
            //run once after all tests are run
            //this is NOT run once-per-test
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            //run before each test is run
            //this IS run one-per-test
            DeleteLocalDBData();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            //run after each test is run
            //this IS run one-per-test
        }

        private static void DeleteLocalDBData()
        {
            DbProviderFactory providerFactory = DbProviderFactories.GetFactory("System.Data.OleDb");

            DbConnection connection = providerFactory.CreateConnection();
            connection.ConnectionString = destinationConnectionString;
            connection.Open();

            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "DELETE FROM Appointments";
                command.ExecuteNonQuery();

                command.CommandText = "DELETE FROM Resources";
                command.ExecuteNonQuery();
            }
        }

        private static void CreateLocalDBSchema()
        {
            DbProviderFactory providerFactory = DbProviderFactories.GetFactory("System.Data.OleDb");

            DbConnection connection = providerFactory.CreateConnection();
            connection.ConnectionString = destinationConnectionString;
            connection.Open();

            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = XtraSchedulerSchemaDDL;
                command.ExecuteNonQuery();
            }
        }

        private static void SetupAdsSourceSettings()
        {
            adsSourceSettings = new DatabaseSettings();
            PreparedSettings.SetupAdsSourceSettings(adsSourceSettings);
        }

        private static void SetupJetSourceSettings()
        {
            jetSourceSettings = new DatabaseSettings();
            PreparedSettings.SetupJetSourceSettings(jetSourceSettings);
        }

        private static void SetupLocalDBDestinationSettings()
        {
            localDBDestinationSettings = new DatabaseSettings();
            PreparedSettings.SetupLocalDBDestinationSettings(localDBDestinationSettings, destinationConnectionString);
        }
                
        [TestMethod]
        public void TestImportingFromAccess()
        {
            using (ImportEngine engine = new ImportEngine())
            {
                engine.ImportSchedulerData(jetSourceSettings, localDBDestinationSettings, ProgressCallback);
            }

            TestResultsOfImportingResources();
            TestResultsOfImportingAppointments();
        }

        private void ProgressCallback(byte percentComplete)
        {
            //specified for code coverage
        }

        [TestMethod]
        public void TestImportingFromAdvantage()
        {
            using (ImportEngine engine = new ImportEngine())
            {
                engine.ImportSchedulerData(adsSourceSettings, localDBDestinationSettings, ProgressCallback);
            }

            TestResultsOfImportingAppointments();
        }

        private static void TestResultsOfImportingResources()
        {
            DbProviderFactory providerFactory = DbProviderFactories.GetFactory("System.Data.OleDb");

            DbConnection connection = providerFactory.CreateConnection();
            connection.ConnectionString = destinationConnectionString;
            connection.Open();

            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Resources";
                DbDataReader reader = command.ExecuteReader();

                int resourceCount = 0;

                while (reader.Read())
                {
                    int uniqueID = (int)reader["UniqueID"];
                    string resourceName = (string)reader["ResourceName"];
                    int color = (int)reader["Color"];

                    if (uniqueID == 1)
                    {
                        Assert.AreEqual(resourceName, "Nate");
                        Assert.AreEqual(color, 0);
                    }
                    if (uniqueID == 2)
                    {
                        Assert.AreEqual(resourceName, "Bill");
                        Assert.AreEqual(color, 14982788);
                    }
                    if (uniqueID == 8818)
                    {
                        Assert.AreEqual(resourceName, "Joe");
                        Assert.AreEqual(color, 13952740);
                    }

                    resourceCount++;
                }

                Assert.AreEqual(resourceCount, 3);
            }
        }

        private static void TestResultsOfImportingAppointments()
        {
            DbProviderFactory providerFactory = DbProviderFactories.GetFactory("System.Data.OleDb");

            DbConnection connection = providerFactory.CreateConnection();
            connection.ConnectionString = destinationConnectionString;
            connection.Open();

            using (DbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Appointments";
                DbDataReader reader = command.ExecuteReader();

                int appointmentCount = 0;

                while (reader.Read())
                {
                    int uniqueID = (int)reader["UniqueID"];
                    int type = (int)reader["Type"];
                    DateTime startDate = (DateTime)reader["StartDate"];
                    DateTime endDate = (DateTime)reader["EndDate"];
                    bool allDay = reader["AllDay"] == DBNull.Value ? false : (bool)reader["AllDay"];
                    object status = reader["Status"];
                    object label = reader["Label"];
                    string resourceIDs = (string)reader["ResourceIDs"];
                    int resourceCount = Regex.Matches(resourceIDs, "ResourceId Type").Count;
                    object recurrenceInfo = reader["RecurrenceInfo"];

                    if (uniqueID == 25)
                    {
                        Assert.AreEqual(type, 1);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 26);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, 2);
                        Assert.AreEqual(label, 0);
                        Assert.AreEqual(resourceCount, 1);
                        Assert.IsTrue(Regex.Matches((string)recurrenceInfo, @"<RecurrenceInfo Start=""12/26/2012 13:00:00"" End=""12/26/2012 13:30:00"" Id="".*"" Index=""-1"" />").Count == 1);
                    }
                    if (uniqueID == 26)
                    {
                        Assert.AreEqual(type, 1);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 26);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, 2);
                        Assert.AreEqual(label, 0);
                        Assert.AreEqual(resourceCount, 1);
                        Assert.IsTrue(Regex.Matches((string)recurrenceInfo, @"<RecurrenceInfo Start=""12/26/2012 14:00:00"" End=""02/27/2013 00:00:00"" WeekDays=""42"" Id="".*"" Range=""2"" Type=""1"" Index=""-1"" />").Count == 1);
                    }
                    if (uniqueID == 27)
                    {
                        Assert.AreEqual(type, 1);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 26);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, 2);
                        Assert.AreEqual(label, 0);
                        Assert.AreEqual(resourceCount, 1);
                        Assert.IsTrue(Regex.Matches((string)recurrenceInfo, @"<RecurrenceInfo Start=""12/26/2012 15:00:00"" End=""12/26/2012 15:30:00"" DayNumber=""26"" WeekOfMonth=""0"" WeekDays=""8"" Id="".*"" OccurrenceCount=""10"" Range=""1"" Type=""2"" Index=""-1"" />").Count == 1);
                    }
                    if (uniqueID == 28)
                    {
                        Assert.AreEqual(type, 1);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 26);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, 2);
                        Assert.AreEqual(label, 0);
                        Assert.AreEqual(resourceCount, 1);
                        Assert.IsTrue(Regex.Matches((string)recurrenceInfo, @"<RecurrenceInfo Start=""12/26/2012 16:00:00"" End=""12/26/2012 16:30:00"" DayNumber=""26"" WeekOfMonth=""0"" WeekDays=""8"" Id="".*"" Month=""12"" Type=""3"" Index=""-1"" />").Count == 1);
                    }
                    if (uniqueID == 29)
                    {
                        Assert.AreEqual(type, 0);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 26);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, 2);
                        Assert.AreEqual(label, 0);
                        Assert.AreEqual(reader["ReminderInfo"], @"<Reminders>
<Reminder AlertTime=""12/26/2012 16:45:00"" />
</Reminders>");
                        Assert.AreEqual(resourceCount, 1);
                    }
                    if (uniqueID == 30)
                    {
                        Assert.AreEqual(type, 0);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 26);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, 2);
                        Assert.AreEqual(label, 1);
                        Assert.AreEqual(resourceCount, 1);
                    }
                    if (uniqueID == 31)
                    {
                        Assert.AreEqual(type, 0);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 26);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, 2);
                        Assert.AreEqual(label, 0);
                        Assert.AreEqual(resourceCount, 2);
                    }
                    if (uniqueID == 32)
                    {
                        Assert.AreEqual(type, 3);
                        Assert.AreEqual(startDate.Day, 27);
                        Assert.AreEqual(endDate.Day, 27);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, DBNull.Value);
                        Assert.AreEqual(label, DBNull.Value);
                        Assert.AreEqual(resourceCount, 1);
                        Assert.IsTrue(Regex.Matches((string)recurrenceInfo, @"<RecurrenceInfo Start=""12/27/2012 14:00:00"" End=""12/27/2012 14:30:00"" Id="".*"" Index=""1"" />").Count == 1);
                    }
                    if (uniqueID == 33)
                    {
                        Assert.AreEqual(type, 3);
                        Assert.AreEqual(startDate.Day, 28);
                        Assert.AreEqual(endDate.Day, 28);
                        Assert.AreEqual(allDay, false);
                        Assert.AreEqual(status, DBNull.Value);
                        Assert.AreEqual(label, DBNull.Value);
                        Assert.AreEqual(resourceCount, 1);
                        Assert.IsTrue(Regex.Matches((string)recurrenceInfo, @"<RecurrenceInfo Start=""12/28/2012 15:00:00"" End=""02/27/2013 00:00:00"" WeekDays=""42"" Id="".*"" Range=""2"" Type=""1"" Index=""1"" />").Count == 1);
                    }
                    if (uniqueID == 34)
                    {
                        Assert.AreEqual(type, 0);
                        Assert.AreEqual(startDate.Day, 26);
                        Assert.AreEqual(endDate.Day, 27);
                        Assert.AreEqual(allDay, true);
                        Assert.AreEqual(status, 0);
                        Assert.AreEqual(label, 0);
                        Assert.AreEqual(resourceCount, 1);
                    }

                    appointmentCount++;
                }

                Assert.AreEqual(appointmentCount, 10);
            }
        }
    }
}
